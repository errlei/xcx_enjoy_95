import http from 'wechat-http'

// 添加基地址
http.baseURL = "https://live-api.itheima.net/"

http.intercept.request = function(options) {
  const defaultOptions = {}
  if(getApp().token) {
    defaultOptions.Authorization = getApp().token
  }
  options.header = Object.assign({}, defaultOptions,options.header)
  // defaultOptions.header.Authorization
  // options.header.Authorization = getApp().token 不能这样连写，因为opitons参数里面没有header属性
  return options
}

// 响应拦截器，添加以后，之前写过的所有接口，都需要将res.data.xxx 中间的data给去掉，因为这里进行了脱壳处理
http.intercept.response = async function(res) {
  // token失效 状态码返回401. 如果刷新token也失效返回状态码也是401.如果不单独处理就会死循环
  if(res.data.code === 401) {
    // 进入这个401有两个情况可以进来，token失效 + 刷新token失效
    if(res.config.url.includes('/refreshToken')) {
      // 只要401进入了这个if分支，就证明刷新token也失效了，那我们就不能在做无感刷新了，直接退出到登录页面，让用户重新登录 （tokne过期时间4小时。refreshToken刷新token过期时间一般3天）
      wx.navigateTo({
        url: '/pages/login/index',
      })
      return
    }
    // 如果是以前，在这里监听到token过期以后，人资里面是清空token，在跳转到登录页面。重新登录
    // 我们小程序这里后端给我们写了个 根据刷新token获取 新token的接口，所以我们可以使用更优化的方式
    // 无感刷新
    const app = getApp()
    // console.log(app.refreshToken);

    const res1 = await http({
      url: '/refreshToken',
      method: 'POST',
      header: {
        Authorization: app.refreshToken
      }
    })
    // console.log('使用刷新token去发请求，', res1);
    getApp().setToken(res1.data.token,res1.data.refreshToken)

    // 这里在获取了新的token以后，将之前因为token实现的请求，再发送一遍，这个过程叫无感刷新，用户对于token失效是没有感觉
    // console.log(res.config);
    // http(res.config)
    http(Object.assign(res.config, {header: {Authorization: getApp().token}}))
  }
  return res.data
}

// 两种方式导出去
wx.http = http
export default http