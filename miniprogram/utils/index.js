const utils = {
  // 二次封装toast提示框
  toast(title = "数据加载失败...") {
    wx.showToast({
      title,
      icon: 'none',
      duration: 2000,
      mask: true
    })
  }
}

// 微信小程序里面，封装的方法想让其他页面或者组件使用有两种办法
wx.utils = utils

// 第二种导出方法-- es5
export default utils