Component({
  data: {
    // 用户是否登录 （是根据有没有token值来判断的）
    isLogin: false
  },
  lifetimes: {
    attached(){
      console.log(getApp());
      // 因为isLogin是布尔值，为了代码的严谨，应该吧所有的变量转换为布尔值，用!!
      const token = getApp().token
      this.setData({
        isLogin: !!token
      })

      // 如果没有登录，用编程式导航，跳转到登录界面去登录
      if(!this.data.isLogin) {
        // 在跳转到登录页面的时候，将当前页面的路径传递给登录页面，等我们登录成功以后，在调回去之前的记录页面，而不是跳回首页
        const pages = getCurrentPages()
        console.log(pages);
        const currentRoute = pages[pages.length - 1].route
        // 因为页面栈残留的问题，这里的路由跳转，选择redirectTo，不要使用navigatorTo
        // A-B-C  使用redirectTo到D页面 ，页面栈就会变成  A-B-D
        // A-B-C  使用navigatorTo到D页面 ，页面栈就会变成  A-B-C-D
        wx.redirectTo({
          url: `/pages/login/index?route=/${currentRoute}`,
        })
      }
    }
  }
  // 组件的生命周期应该写在lifetimes里面，新的语法，下面的语法也可以，但是不推荐
  // attached(){}
})