let pid =''
Page({
  data: {},
  onLoad({id: id}){
    this.getDetail(id)
    // id = id 这里不能这么写，因为函数有一个参数是id，然后函数会有限使用自己的形参，不会使用全局变量
    pid = id
    console.log('详情页面id：', id);
  },
  async getDetail(id) {
    const res = await wx.http.get(`/room/${id}`)
    console.log(res);
    // data上面的值，开始并不需要在data上面声明，只需要setData里面设置了key以后，就会自动给data上面添加对应属性
    this.setData({...res.data})
  },
  editHouse() {
    console.log(`/house_pkg/pages/form/index?id=${pid}`);
    wx.navigateTo({
      url: `/house_pkg/pages/form/index?id=${pid}`,
    })
  },
})
