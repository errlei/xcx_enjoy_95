import qqMap from '../../../utils/qqmap'

Page({
  data: {
    address: '',
    list: []
  },
  onLoad() {
    this.getMyLocation()
  },
  async getMyLocation() {
    const res = await wx.getLocation()
    this.transfromCn(res.latitude, res.longitude)
    // 获取当前定位周边的建筑物
    this.getNeightSheQu(res.latitude,res.longitude)
  },
  getNeightSheQu(latitude,longitude ) {
    console.log(333333);
    qqMap.search({
      keyword: '社区',
      page_size: 5,
      location: [latitude,longitude].join(','),
      success: (res) => {
        // 将服务器返回的数据加工以下，因为setData最大支持1MB，修改数据的时候传递的参数越小越好
        const tmp = res.data.map(item => ({
          id: item.id,
          title:item.title
        }))
        // 渲染页面
        this.setData({
          list: tmp
        })
      }
    })
  },
  async chooseMyLocation(){
    const res = await wx.chooseLocation()
    this.transfromCn(res.latitude, res.longitude)
  },
  transfromCn(latitude, longitude) {
    qqMap.reverseGeocoder({
      // location: {
      //   latitude: res.latitude,
      //   longitude: res.longitude
      // },
      location: latitude + ',' + longitude,
      success: (res) => {
        this.setData({
          address: res.result.address
        })
      }
    })
  }
})