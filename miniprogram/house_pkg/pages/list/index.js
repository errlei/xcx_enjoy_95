let id = ''

Page({
  data: {
    // id: '', 因为这个id不需要在页面渲染，不需要在data上面定义，
    dialogVisible: false,
    list: []
  },
  // onLoad() {
  onShow() {
    this.getHouseList()
  },
  async getHouseList() {
    const res = await wx.http.get('/room')
    console.log(111, res);
    this.setData({
      list: res.data
    })
  },
  swipeClose(ev) {
    console.log(ev.mark.id);
    id = ev.mark.id
    const { position, instance } = ev.detail

    if (position === 'right') {
      // 显示 Dialog 对话框
      this.setData({
        dialogVisible: true,
      })

      // swiper-cell 滑块关闭
      instance.close()
    }
  },

  goDetail(e) {
    // console.log(e);
    // e.target 表示当前触发这个事件的元素  e.currentTarget 绑定事件的元素
    // console.log(e.target.dataset.id, e.currentTarget.dataset.id);
    // console.log(e.mark.aaa);
    wx.navigateTo({
      url: `/house_pkg/pages/detail/index?id=${e.mark.aaa}`,
    })
  },

  addHouse() {
    wx.navigateTo({
      url: '/house_pkg/pages/locate/index',
    })
  },
  dialogClose(e) {
    console.log(e.detail);
  },
  async delHouse() {
    // 下面是本地删除数据的写法，不需要发请求
    // this.data.list.splice(索引号, 1)

    const res = await wx.http.delete(`/room/${id}`)
    console.log('开始删除：', res);
    if (res.code !== 10000) {
      wx.utils.toast(res.message || '删除失败')
      return
    }
    // 删除成功执行这里的代码
    this.getHouseList()
  }
})
