Page({
  data: {
    // size 当前小区一共有多少栋楼
    size: 5,
    // 上一个页面传递进来的小区名称
    point: ''
  },
  onLoad(options) {
    console.log('上一个页面传递进来的query参数：', options);
    this.fake(options.point)
  },
  fake(point) {
    // 一般小区20栋楼， 随机数获取 5 - 20 随机数
    // [0,1)   * 16 ----  [0, 16)   +5  ---- [5, 21)
    // 10  20
    const size = Math.floor(Math.random()*16)  + 5
    this.setData({
      point: point,
      size: size
    })
  }
})