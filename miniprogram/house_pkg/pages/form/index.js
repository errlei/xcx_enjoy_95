// \u4e00-\u9fa5] 中文验证规则

Page({
  data: {
    // idcardFrontUrl: '/static/images/avatar_1.jpg',
    // idcardBackUrl: '/static/images/avatar_2.jpg',
    idcardFrontUrl: '',
    idcardBackUrl: '',
    point: '',
    building: '',
    room: '',
    name: '',
    gender: '0',
    mobile: ''
    // form: {
    //   point:,
    //   room: 
    // }
  },
  // onLoad(options){
  onLoad({point, room, building, id}){
    // 编辑的时候id有值，新增的时候没有值，等下可以借助这个id的值，判断当前页面是 编辑还是新增
    console.log(point, room, building, id);
    this.init(point, room, building, id)
  },
  async init(point, room, building, id) {
    if (id) {
      wx.setNavigationBarTitle({
        title: '编辑房屋信息'
      })
      // id有值就是编辑的逻辑
      const res = await wx.http.get(`/room/${id}`)
      console.log(res);
      // 如果像下面这么写，性别字段后端这里返回的又是数字，我们需要的是字符串
      // this.setData({...res.data})
      this.setData(Object.assign({}, {...res.data}, {gender: res.data.gender.toString()}))
    } else {
      // 这里是新增的逻辑代码
      this.setData({
        point,
        building,
        room
      })
    }
  },
  updateImg(e) {
    const that = this
    console.log(e.currentTarget.dataset.type);
    // 上传图片的api
    wx.chooseMedia({
      count: 9,
      mediaType: ['image','video'],
      sourceType: ['album', 'camera'],
      maxDuration: 30,
      camera: 'back',
      success(res) {
        console.log(res.tempFiles[0].tempFilePath)
        wx.uploadFile({
          url: 'https://live-api.itheima.net/upload',
          filePath: res.tempFiles[0].tempFilePath,
          name: 'file',
          header: {
            Authorization: getApp().token
          },
          success (res){
            console.log(res);
            const tmp = JSON.parse(res.data)
            console.log(tmp.data?.url);
            that.setData({
              // idcardFrontUrl: tmp.data?.url
              [e.currentTarget.dataset.type]: tmp.data?.url
            })
          }
        })
      }
    })
  },
  async goList() {
    // 先校验成功，在发请求，在跳转
    if(!this.validateName() || !this.validateMobile() || !this.validateImg()) return

    const data = {
      point: this.data.point,
      building: this.data.building,
      room: this.data.room,
      name: this.data.name,
      gender: this.data.gender,
      mobile: this.data.mobile,
      idcardFrontUrl: this.data.idcardFrontUrl,
      idcardBackUrl: this.data.idcardBackUrl,
    }

    const postData = this.data.id ? Object.assign(data, {id: this.data.id}) : data

    // wx.http.post('/room', {xxxxx})
    const res = await wx.http({
      url: '/room',
      method: 'POST',
      // data: this.data
      data: postData
    })

    console.log('请求成功',res);
    if(res.code !== 10000) {
      wx.utils.toast('新增或编辑房屋失败')
      return
    }

    wx.navigateBack({
      delta: this.data.id ? 2 : 4
    })
    // reLaunch相当于重启的意思，既可以跳转到tabbar页面，也可以跳转到非tabbar页面。会清空所有页面栈
    // wx.reLaunch({
    //   url: '/house_pkg/pages/list/index',
    // })
  },
  removePicture(ev) {
    // 移除图片的类型（身份证正面或反面）
    const type = ev.mark?.type
    this.setData({ [type]: '' })
  },
  // 下面是三个字段的校验函数，要自己写
  validateName() {
    // 姓名必须是2-5个汉字
    const pattern = /^[\u4e00-\u9fa5]{2,5}$/
    if(!pattern.test(this.data.name)) {
      wx.utils.toast('姓名校验失败')
      return false
    }
    return true
  },
  validateMobile() {
    // 姓名必须是2-5个汉字
    const pattern = /^1[356789]\d{9}$/
    if(!pattern.test(this.data.mobile)) {
      wx.utils.toast('手机号校验失败')
      return false
    }
    return true
  },
  validateImg() {
    if(!this.data.idcardFrontUrl || !this.data.idcardBackUrl) {
      wx.utils.toast('身份证正反面照片必传')
      return false
    }
    return true
  }
})
