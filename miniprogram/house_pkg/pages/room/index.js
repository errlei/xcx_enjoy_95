Page({
  data: {
    point: '',
    building: '',
    room: []
  },
  // 函数参数的解构赋值
  onLoad({point, building}) {
    this.fake(point, building)
  },
  fake(point, building) {
    // 当前页面一共有多少个房间，自己构造一个假数据，随机模仿服务器返回5-20条数据
    const size = Math.floor(Math.random() * 16) + 5

    // 下面的for循环就是根据siez随机生成若干个房间号
    for(let i=0; i<size; i++) {
      // 下面就是往room数组里面 添加101房间号，size有多大，就有多少个房间号
      // Math.floor(Math.random()* (max-min+1)) + min
      // 房间号都是由楼层+房间号码决定的 1 - 33
      const floor = Math.floor(Math.random()* 33) + 1
      // 每一层房间的号码， 一般小区一层只有4个房间  1 - 4
      const roomNumber = Math.floor(Math.random()* 4) + 1
      const tmpRoom = floor +'0' + roomNumber
      this.data.room.push(tmpRoom)
    }
    this.setData({
      point,
      building,
      room: this.data.room
    })
  }
})