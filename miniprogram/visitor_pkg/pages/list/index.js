Page({
  data: {
    list: []
  },
  onLoad() {
    this.getVisitorList()
  },
  async getVisitorList() {
    const res = await wx.http.get('/visitor?current=1&pageSize=10')
    console.log(res);
    this.setData({
      list: res.data.rows
    })
  },
  goPassport(e) {
    console.log(e.mark.id);
    wx.navigateTo({
      url: `/visitor_pkg/pages/passport/index?id=${e.mark.id}`,
    })
  },
})
