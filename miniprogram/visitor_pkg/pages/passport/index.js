Page({
  onLoad({id}) {
    console.log('进入了passport页面', id);
    this.getDetail(id)
  },
  async getDetail(id) {
    const res = await wx.http.get(`/visitor/${id}`)
    console.log(res);
    this.setData({...res.data})
  },
  // 两个地方可以触发这个事件， 1. 右上角的分享按钮 2.button的open-type=“share”
  // 点击了分享，自动触发事件onShareAppMessage
  onShareAppMessage() {
    return {
      title: '查看通行证',
      path: '/visitor_pkg/pages/passport/index',
      imageUrl: 'https://enjoy-plus.oss-cn-beijing.aliyuncs.com/images/share_poster.png',
    }
  },
  saveToLocal() {
    // 这一步是将网络地址转换为本地临时路径地址
    // const res = await wx.getImageInfo({
    //   src: this.data.url,
    // })
    // console.log(res);
    // // 因为下面这个保存图片到相册的api，不能直接存网络图片
    // wx.saveImageToPhotosAlbum({
    //   // filePath: this.data.url,
    //   filePath: res.path,
    // })

    // 通过下面这个api，也是生成一个临时路径，在给saveImageToPhotosAlbum这个api使用
    wx.downloadFile({
      url: this.data.url,
      success: (res) => {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
        })
      }
    })
  }
})
