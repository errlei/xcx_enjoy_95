// 在js文件里面写一个全局变量，因为这个code数据，页面上不需要使用，我们就不用定义在data上面
// 定义在Page构造函数外面，这个页面里面所有的函数都可以使用这个变量
let secret_code = ''
Page({
  data: {
    countDownVisible: false,
    mobile: '',
    code: ''
  },
  onLoad(options){
    console.log('我是登录页面：',options);
    // 如果其他函数需要互相使用对方的变量，比较好的方式是挂载到this上面
    // 因为所有的函数都可以访问同一个this对象。页面无关的数据尽量不要方到data上面
    this.route = options.route
  },
  async login(){
    // 登录发请求之前，先校验手机号和验证码的格式，
    if (!this.validateMobile() || !this.validateCode()) return
    
    const res = await wx.http.post('/login', {mobile: this.data.mobile, code: this.data.code})
    // 请求发送完毕，先处理业务失败，再去写成功的逻辑
    if (res.code !== 10000) {
      wx.utils.toast(res.message || '登录请求失败')
      return
    }
    // 下面要将token保存起来，保存到两个地方，一个是getApp，一个是本地储存 （类似于vue里面的 vuex和localStorage）
    // token是后端给我们的，一般有过期时间，出于安全性考虑的。如果token在过期事情之后过期了，可以情况token，强行跳转到登录页面。这种用户的体验不好。我们是为了增加用户的体验。就用了一个刷新token。这个值就是专门用来自动更新token的。如果token过期，自动使用刷新token去发请求，获取新的token，在自动的把之前因为token失效的请求在发送一遍，达到一个无感刷新的功能
    getApp().setToken(res.data.token,res.data.refreshToken)

    // console.log('在登录函数里面获取route的值');
    wx.navigateTo({
      url: this.route,
    })
  },
  validateMobile() {
    // 正则校验手机号
    const pattern = /^1[356789]\d{9}$/
    if(!pattern.test(this.data.mobile.trim())) {
      // 校验函数先写假值的校验， 利用return语句，可以少些else语句，简化代码
      wx.utils.toast('手机号格式错误')
      return false
    }

    return true
  },
  validateCode(){
    const pattern = /^\d{6}$/
    if(!pattern.test(this.data.code)){
      wx.utils.toast('验证码格式错误')
      return false
    }

    return true
  },
  async getCode(){
    // 获取验证码的请求，先要校验，校验通过以后在发请求
    // return 后面既可以写值，也可以不写，如果使用函数，需要用到他的返回值就写reutnr xxx
    // 如果不写的话，这个函数返回值就是 undefined
    if(!this.validateMobile()) return
    const res = await wx.http.get(`/code?mobile=${this.data.mobile.trim()}`)
    
    // 请求结束以后，还需要做一些业务成功或失败的校验
    // 面试项目自我介绍的时候，可以说，我们公司的后端(其他前端，ui，测试沟通)他们规定请求有业务成功和业务失败
    // 他们是java同事规定，业务成功返回code码是10000，如果是失败就是非10000。我们在前端代码里面再去判断给提示即可
    if(res.code !== 10000) {
      wx.utils.toast(res.message || '获取验证码失败')
      return
    }
    wx.utils.toast('获取验证码成功')
    console.log(res.data.code);
    secret_code = res.data.code
    this.setData({
      countDownVisible: true
    })
  },
  setCode() {
    // 下面这个函数是将数据放到剪切板里面， 类似与我们手指的 ctrl +c
    // 这些api方法是 宿主微信给我们提供的，如果开发前端web程序，需要自己下载读取剪贴板插件---重点-提升用户体验
    wx.setClipboardData({
      data: secret_code,
    })

    // 获取剪贴板的数据，wx.getClipboardData()什么时候使用
    // 进入拼多多的时候，如果你在微信复制了朋友发给你的拼多多的链接。已进入拼多多，这个程序就会主动读取你的剪贴板
    // 拼多多页面就会打开一个弹出框，引导你去朋友分享的那个商品页面
  },
  countDownChange(ev) {
    this.setData({
      timeData: ev.detail,
      countDownVisible: ev.detail.minutes === 1 || ev.detail.seconds > 0,
    })
  },
})
