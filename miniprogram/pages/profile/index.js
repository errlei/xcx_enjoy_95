const app = getApp()
Page({
  data: {
    avatar: '',
    nickName:''
  },
  onLoad() {
    this.getProfile()
  },
  getProfile() {
    // 在my页面我们是发请求获取数据，在这个页面直接从app.js的全局状态里面获取数据
    this.setData({
      avatar: app.userInfo.avatar,
      nickName: app.userInfo.nickName
    })
  },
  async updateNickName(e){
    console.log(e.detail.value);
    if (!e.detail.value) {
      wx.utils.toast('昵称不能为空')
      return
    }

    const res = await wx.http.put('/userInfo', {nickName: e.detail.value})

    if(res.code !== 10000) {
      wx.utils.toast('更新昵称请求失败')
      return
    }

    wx.utils.toast('更新成功')
    app.userInfo.nickName = e.detail.value
  },
  chooseavatarUpdate(e) {
    // https://airandomimage.art/
    console.log(e.detail.avatarUrl);
    wx.uploadFile({
      filePath: e.detail.avatarUrl,
      name: 'file',
      url: 'https://live-api.itheima.net/upload',
      header: {
        Authorization: app.token
      },
      // 上传图片，参数key不用写data，需要写formData
      formData: { type: 'avatar'},
      success: (res) => {
        // 状态码是200就进入这个success， 业务成功是10000，非10000就是业务失败
        // console.log('上传成功', res);
        const data = JSON.parse(res.data)
        console.log(data);
        if(data.code !== 10000) {
          wx.utils.toast(data.message || '上传图片业务失败')
          return
        }
        // 如果数据更新成功，将图片更新到当前页面上，以及appjs里面
        this.setData({
          avatar: data.data.url
        })
        app.userInfo.avatar = data.data.url
      },
      fail(err) {
        // 进入这里的都是http状态吗 4xx 5xx
        console.log('请求失败', err);
      }
    })
  }
})