// import http from '../../utils/http'
Page({
  data: {
    list:[]
  },
  onLoad() {
    this.getList()
  },
  async getList() {
    // const res = await wx.http({method: 'GET', url: '/announcement' })
    const res = await wx.http.get('/announcement')
    console.log(res);
    this.setData({
      list: res.data
    })
  }
})
