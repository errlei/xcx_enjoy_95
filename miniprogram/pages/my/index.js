const app = getApp()
Page({
  data: {
    avatar: '',
    nickName: ''
  },
  goLogin() {
    wx.navigateTo({
      url: '/pages/login/index',
    })
  },
  // onLoad不行，替换为onSHow
  onShow() {
    // onLoad在页面只会执行一次，如果跳转到非tabar页面，会缓存到内存里面
    // 在回来这个页面就不会在执行onLoad函数。 onShow函数只要页面显示就会一直执行
    this.getProfile()
  },
  async getProfile() {
    // 401状态码 表示token失效或者错误意思  403 表示用户没有权限  404 访问不到资源
    // token 就是我们用 账号和密码 登录成功以后，换取的一个身份凭据。 后面所有的请求都带上这个token，就表示我们登录过了，别人请求才能返回正确的数据
    const res = await wx.http.get('/userInfo')
    console.log(res);
    this.setData({
      avatar: res.data?.avatar,
      nickName: res.data?.nickName
    })
    app.userInfo.avatar = res.data?.avatar
    app.userInfo.nickName = res.data?.nickName
  }
})
