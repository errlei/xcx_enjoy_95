Page({
  data: {
    detail: {}
  },
  onLoad(option) {
    console.log('这里获取上一个页面传递进来的参数：', option);
    this.getDetail(option.id)
  },
  async getDetail(id){
    const res = await wx.http.get(`/announcement/${id}`)
    console.log(res);
    this.setData({
      detail: res.data
    })
  }
})