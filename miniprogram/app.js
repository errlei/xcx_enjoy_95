// es5的模块化规范，import 有时候不需要导入一个变量，只需要执行这个文件，让这个文件生效即可
import  './utils/index'
import './utils/http'

// wx.utils.toast()

// import utils from './utils/index'
// utils.toast()

App({
  // 在其他页面使用getApp访问token数据，因为在内存里面，速度比较快，所以使用全局appjs的数据
  token: '',
  refreshToken : '',
  userInfo: {
    avatar: '',
    nickName: ''
  },
  onLaunch() {
    this.getToken()
  },
  getToken() {
    // 本地储存是因为每次一刷新 app上面的数据都会清空，所以一般是 app.js上面的数据结合本地储存一起用
    // vue也是一样的，全局数据储存在vuex里面，数据在内存里访问比较快，但是一刷新数据就没有了，所以结合本地储存一起用
    wx.getStorage({
      key: 'token',
      success: (res) => {
        this.token = res.data
      }
    })
    this.refreshToken = wx.getStorageSync('refreshToken')
  },
  setToken(token, refreshToken) {
    this.token = 'Bearer ' + token
    this.refreshToken = 'Bearer ' + refreshToken
    wx.setStorage({
      key: 'token',
      data: 'Bearer ' + token
    })
    wx.setStorage({
      key: 'refreshToken',
      data: 'Bearer ' + refreshToken
    })
  }
})
