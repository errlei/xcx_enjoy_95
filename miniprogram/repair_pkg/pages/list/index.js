Page({
  data: {
    list: [],
    total: 0
  },
  onLoad() {
    this.getRepaireList()
  },
  async getRepaireList() {
    const res = await wx.http.get(`/repair?current=1&pageSize=10`)
    console.log(res);
    this.setData({
      list: res.data.rows,
      total: res.data.total
    })
  },
  goDetail(e) {
    // data-xxxx传参 获取就是用 e.currentTarget.dataset.id
    console.log(e.mark.id);
    wx.navigateTo({
      url: `/repair_pkg/pages/detail/index?id=${e.mark.id}`,
    })
  },
  addRepair() {
    wx.navigateTo({
      url: '/repair_pkg/pages/form/index',
    })
  },
})
