let pid = '';
Page({
  data: {
    latitude: 30.70736,
    longitude: 114.400578,
    markers: [
      {
        id: 1,
        title: '黑马',
        latitude: 30.70736,
        longitude: 114.400578,
        width: 20,
        height: 20,
        customCallout: {
          anchorY: 10,
          anchorX: 0,
          display: 'ALWAYS',
        }
      	// iconPath: '../../static/uploads/attachment.jpg',
      },
      {
        id: 2,
        latitude: 30.70736,
        longitude: 114.401683,
        width: 20,
      	height: 20,
      	// iconPath: '../../static/uploads/attachment.jpg',
      },
      {
        id: 2,
        latitude: 30.70936,
        longitude: 114.400578,
        width: 20,
      	height: 20,
      	// iconPath: '../../static/uploads/attachment.jpg',
      }
    ],
    isShow: false
  },
  onLoad({id: id}) {
    console.log('详情页面：', id);
    this.getRepaireDetail(id)

    pid = id
    this.id = id
  },
  async getRepaireDetail(id) {
    const res = await wx.http.get(`/repair/${id}`)
    console.log(res);
    this.setData({...res.data})
  },
  cancelRepaire() {
    this.setData({
      isShow: true
    })
  },
  async dialogConfirm() {
    const res = await wx.http.put(`/cancel/repaire/${pid}`)  // this.id
    if(res.code !== 10000) {
      wx.utils.toast(res.message || '取消报修请求失败')
      return
    }
    wx.reLaunch({
      url: '/repair_pkg/pages/list/index',
    })
  },
  // 修改信息的
  editRepaire() {
    wx.navigateTo({
      url: `/repair_pkg/pages/form/index?id=${pid}`,
    })
  }
})
